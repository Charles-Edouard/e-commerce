var path = require("path")

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return new Promise((resolve, reject) => {
    const StoreTemplate = path.resolve("src/templates/details.js")
    const BlogTemplate = path.resolve("src/templates/blogDetails.js")
    resolve(
      graphql(`{
          allProduct(sort: {fields: flotiqInternal___createdAt, order: DESC}) {
            edges{
              node{
                id
                slug,
                name,
                price,
                description,
                blogPost{
                  id,
                  slug
                },
                productImage {
                  id,
                  extension
                },
                productGallery {
                  id,
                  extension
                }
              }
            }
          
        }
        allBlogpost{
          edges{
            node{
              id
              slug,
              title,
              content,
              excerpt,
              thumbnail {
                id
                extension
              },
              headerImage {
                id
                extension
              }
            }
          }
        }  
      }
        
      `).then(result => {
        if (result.errors) {
          reject(result.errors)
        }

        const products = result.data.allProduct.edges;

        products.forEach((edge, index) => {

          const previous = index === products.length - 1 ? null : products[index + 1].node;
          const next = index === 0 ? null : products[index - 1].node;

          createPage({
            path: edge.node.slug,
            component: StoreTemplate,
            context: {
              slug: edge.node.slug,
              previous,
              next
            },
          })
        });
        const blogs = result.data.allBlogpost.edges;

        blogs.forEach((edge,index) => {
          const previous = index === blogs.length - 1 ? null : blogs[index + 1].node;
          const next = index === 0 ? null : blogs[index - 1].node;
          createPage({
            path: edge.node.slug,
            component: BlogTemplate,
            context: {
              slug: edge.node.slug,
              previous,
              next
            },
          })
        });
        return
      })
    )
  })
}
