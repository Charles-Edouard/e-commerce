var dotenv = require("dotenv");
dotenv.config();

const { spaceId, accessToken, snipcart } = process.env;

module.exports = {
  siteMetadata: {
    title: `Facilit Minceur`,
    description: `Facilit Minceur, une équipe de passionné à votre écoute. Nous proposons des programmes minceurs, perte de poids`,
    author: `Toutain Charles-Edouard`,
  },
  plugins: [{
      "resolve": "gatsby-source-flotiq",
      "options": {
        "baseUrl": process.env.GATSBY_FLOTIQ_BASE_URL,
        "authToken": process.env.GATSBY_FLOTIQ_API_KEY,
        "forceReload": false,
        "includeTypes": ['product', '_media','blogpost','page','project']
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Facilit Minceur`,
        short_name: `Facilit Minceur`,
        start_url: `/`,
        background_color: `#F2380F`,
        theme_color: `#F2380F`,
        display: `standalone`,
        icon: `src/images/icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-snipcart-advanced`,
      options: {
          version: '3.0.15',
          publicApiKey: process.env.SNIPCART_API_KEY, // use public api key here or in environment variable
          defaultLang: 'fr',
          currency: 'eur',
          openCartOnAdd: true,
          locales: {
            fr: {
              actions: {
                checkout: 'Valider le panier',
              },
            }
          },
          innerHTML: `
          <billing section="bottom">
              <!-- Customization goes here -->
          </billing>`,
      },
  },
    {
      resolve: `gatsby-plugin-offline`,
    }
  ],
}
