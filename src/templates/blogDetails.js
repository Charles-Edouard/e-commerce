import React from "react"
import { Tab, Tabs, TabList } from 'react-tabs';
import { graphql} from "gatsby";

import Layout from "../components/layout"
import SEO from "../components/seo"

const BlogDetails = data => (
  < Layout >

    <SEO title={data.data.blogpost.name} keywords={[`gatsby`, `application`, `react`]} />
    <div className="container details-page">
      <div className="blogpost-details">
      {data.data.blogpost.headerImage && data.data.blogpost.headerImage.length ?
            <Tabs>
              <TabList>
                {data.data.blogpost.headerImage.map(items => (
                  <Tab key={items.id}>
                    <img
                        alt={data.data.blogpost.name}
                        src={`${process.env.GATSBY_FLOTIQ_BASE_URL}/image/1920x0/${items.id}.${items.extension}`} />
                  </Tab>
                ))}
              </TabList>
            </Tabs> :
            <div className="no-image">No Image</div>
          }
      <div>
          <h2>{data.data.blogpost.title}</h2>

        
      </div>
      <div
          dangerouslySetInnerHTML={{
            __html: data.data.blogpost.content
          }}
        />
      </div>
    </div>
  </Layout >
)

export default BlogDetails

export const query = graphql`
  query blogBySlug($slug: String!) {
      blogpost(slug: {eq: $slug }) {
        id,
        slug,
        title,
        content,
        excerpt,
        thumbnail {
          id
          extension
        },
        headerImage {
          id
          extension
        }
      }
  }
`
