import React, { Component } from "react";
import Slider from "react-slick";
import Img from "gatsby-image";
import { Link } from "gatsby";

var settings = {
  dots: true,
  speed: 500,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 3000,
  slidesToShow: 1,
  slidesToScroll: 1
};

export default class Banner extends Component {
  render() {

    const { BannerData } = this.props;

    return (
      <div className="slider-section">
        <Slider {...settings}>
          {BannerData.map((items, i) => (
            <div key={i} className="item">
              <div className="site-Banner">
              { items.node.gallery && items.node.gallery[0] ? <Img sizes={{
                      "src": `${process.env.GATSBY_FLOTIQ_BASE_URL}/image/1920x0/${items.node.gallery[0].id}.${items.node.gallery[0].extension}`,
                      "aspectRatio": 1.77,
                      "sizes": '',
                      "srcSet": ''
                  }} /> : <div className="no-image">No Image</div>}
                <div className="Banner-details">
                  <div>
                    <span className="sub-title">{items.node.subHeading}</span>
                    <h1>{items.node.slug}</h1>
                    <Link to="/test">Acheter notre programme test</Link>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}