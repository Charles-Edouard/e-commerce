import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
class About extends React.Component {
    render() {
        const {data} = this.props
        console.log(data)
        return ( <Layout>
            <SEO title={data.page.name} keywords={[`gatsby`, `application`, `react`]} />
                <div className="site-About">
                    <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div
                            dangerouslySetInnerHTML={{
                                __html: data.page.content
                            }}
                            />
                        </div>
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }
}
export default About

export const query = graphql`
  query pageBySlug{
    page(slug: {eq: "about"}) {
        name,
        content
      }
  }
`