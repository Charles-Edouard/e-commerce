import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

class Contact extends React.Component {
    render() {
        return (
            <Layout>
                <SEO title="Contact Us" keywords={[`gatsby`, `Contact Us`, `react`]} />
                <div className="Contact-us">
                    <div className="container">
                        {/* To make form work, use your own formspree credentials in action="" */}
                        <form name="contact" method="POST" data-netlify="true" data-netlify-recaptcha="true">
                        <p class="hidden">
                              <label>Don’t fill this out if you're human: <input name="bot-field" /></label>
                        </p>
                            <div>
                                <label>Votre Nom
                                <input type="text" name="name" required /></label>
                            </div>
                            <div>
                                <label>Votre Email: 
                                <input type="email" name="email" required /></label>
                            </div>
                            <div>
                                <label>Qu'est ce qu'ont peut faire pour vous ?
                                <textarea name="message" required></textarea></label>
                            </div>
                            <div>
                                <label>Un document à transmettre?
                                <input name="document" type="file" ></input></label>
                            </div>
                            <div data-netlify-recaptcha="true"></div>
                            <div>
                                <button type="submit" required>Envoyer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default Contact