import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/layout"
import SEO from "../components/seo"

class IndexPost extends React.Component {

  render() {

    const { data } = this.props;

    return (
      <React.Fragment>
        <div className="row product-main">
          {data.data.allBlogpost.edges.map(items => (
            <div className="Catalogue__item col-sm-12 col-md-12 col-lg-12" key={items.node.id}>
              <div className="details_List">
              <Link to={`/${items.node.slug}`}>
                  { items.node.thumbnail && items.node.thumbnail[0] ? <Img sizes={{
                      "src": `${process.env.GATSBY_FLOTIQ_BASE_URL}/image/1920x0/${items.node.thumbnail[0].id}.${items.node.thumbnail[0].extension}`,
                      "aspectRatio": 1.77,
                      "sizes": '',
                      "srcSet": ''
                  }} /> : <div className="no-image">No Image</div>}

             </Link>
                  <div className="details_inner">

                  <h2>
                    <Link to={`/${items.node.slug}`}>{items.node.title}</Link>
                  </h2>
                  <div className="row">
                    <div className="col-sm-12 align-self-center">
                      <span className="price">{items.node.excerpt}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

const IndexPage = data => (

  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <div className="container">
      <div className="text-center mt-5"><h2 className="with-underline">Tous nos articles</h2></div>
      <IndexPost data={data}></IndexPost>
    </div>
  </Layout>
)

export default IndexPage

export const query = graphql`
  query BlogQuery {
    allBlogpost{
        edges{
          node{
            id
            slug,
            title,
            content,
            excerpt,
            thumbnail {
              id
              extension
            },
            headerImage {
              id
              extension
            }
          }
        }
    }
  }
`



